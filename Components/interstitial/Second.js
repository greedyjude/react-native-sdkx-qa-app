import React from 'react';
import SDKX from 'react-native-sdkx';
import { Text, ToastAndroid, View } from 'react-native';
import { GGButton } from '../../Components/index.js'
import { Actions } from 'react-native-router-flux';

export default function Second() {
    function showAdAndMove() {
        SDKX.loadInterstitialAd("float-5113").then(() => {
            SDKX.showInterstitialAd({
                onAdClosed: () => {
                    Actions.pop()
                }
            })
        }).catch((e) => {
            ToastAndroid.show("Failed to load ad " + e, ToastAndroid.SHORT)
        })
    }
    return (
        <>
            <View>
                <Text>Second Activity</Text>
                <GGButton
                    title="Back"
                    onPress={showAdAndMove}
                />
            </View>
        </>
    )
}