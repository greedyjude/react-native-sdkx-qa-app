import React from 'react';
import { View, TextInput,ToastAndroid } from 'react-native';
import { GGButton } from '..';
import SDKX from 'react-native-sdkx'
import { Actions } from 'react-native-router-flux';
export default function InterstitialMain() {

    const [unitId,setUnitId] = React.useState("float-5114")

    function loadInterstitialAd(){
        SDKX.loadInterstitialAd(unitId).then(()=>{
            ToastAndroid.show("Intersitital ad loaded",ToastAndroid.SHORT)
        }).catch((e)=>{
            ToastAndroid.show("Intersitital ad failed to load "+e,ToastAndroid.SHORT)
        })
    }
    return (
        <>
            <View style={{
                display: "flex",
                flexDirection:"row"
            }}>
                <TextInput
                    value={unitId}
                    placeholder="unitId"
                    style={{
                        flex: 1,
                        borderBottomWidth: 1,
                        margin: 10
                    }}
                    onChangeText={(newValue)=>{
                        setUnitId(newValue)
                    }}
                />

                <GGButton
                    title="New Ad"
                    onPress = {() => {
                         loadInterstitialAd()
                        }
                    }
                />
                <GGButton
                    title="Show Ad"
                    onPress = {() => {
                            SDKX.showInterstitialAd({
                                onAdOpened:()=>{
                                    ToastAndroid.show("On Ad Opened",ToastAndroid.SHORT)
                                },
                                onAdClosed:()=>{
                                    ToastAndroid.show("On Ad Closed",ToastAndroid.SHORT)
                                },
                                onAdLeftApplication:()=>{
                                    ToastAndroid.show("Ad Left Application",ToastAndroid.SHORT)
                                }
                            })
                    }
                }
                />

            </View>
            <GGButton 
            title="First > "
            onPress={()=>{
                Actions.interfirst()
            }} />
        </>
    )
}