import React from 'react';
import { View,Text } from 'react-native';
import { Button } from 'react-native-elements';

export function GGButton(props) {
    return (
        <View style={styles.actionButton}>
            <Button {...props}

                buttonStyle={{ padding: 10, }}
            />
        </View>
    )
}
export function ViewPlaceHolder(props) {
    return (
        <Text
            
                    style={{
                        marginTop:20,                        textAlign:"center",
                        textAlignVertical:"center",
                        color:"#ffffff",
                        backgroundColor:"#800000",
                        height: undefined,
                        width: "100%",
                        aspectRatio: 16 / 9,
                        fontSize:48,
                        fontWeight:'bold'
                    }}
                >(-_-)</Text>
    )
}
  const styles = {
    actionButton: {
        backgroundColor: "#800000",
        marginTop: 10,
        marginHorizontal: 5
    },
}