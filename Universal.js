import React from 'react';
import SDKX, { GGAdView, GGAdviewWrapper } from 'react-native-sdkx';
import { Text, View, TextInput, ToastAndroid } from 'react-native';
import { GGButton, ViewPlaceHolder } from './Components/index.js';


export default function Universal() {
    const [appId, setAppId] = React.useState("81939769")
    const [unitId, setUnitId] = React.useState("float-4732")
    const [isReady, setIsReady] = React.useState(false)
    function initializeSDK() {
        SDKX.initWith({
            appId: appId
        }).then(() => {
            ToastAndroid.show("SDK Initialized", ToastAndroid.SHORT)
            setIsReady(true)
        }).catch((e) => {
            ToastAndroid.show("SDKX init failed " + e, ToastAndroid.SHORT)
            setIsReady(false)
        })
    }

    function loadAdShowInter(){
        SDKX.loadInterstitialAd(unitId).then(()=>{
            SDKX.showInterstitialAd({
                onAdLeftApplication:()=>{
                    ToastAndroid.show("Ad Left app",ToastAndroid.SHORT)
                },
                onAdClosed:()=>{
                    ToastAndroid.show("on ad closed",ToastAndroid.SHORT)
                
                },
                onAdOpened:()=>{
                    ToastAndroid.show("on ad Opened",ToastAndroid.SHORT)
                }
            })
        }).catch((e) => {
            ToastAndroid.show("Failed to load ad "+e,ToastAndroid.SHORT)
        })
    }

    function destroySDK() {
        SDKX.destroy()
        setIsReady(false)
    }
    return (
        <>
            <View
                style={{
                    display: "flex",
                    flexDirection: "column"
                }}
            >
                <TextInput
                    placeholder="App Id"
                    value={appId}
                    style={{
                        borderBottomWidth: 1,
                        minWidth: 0,
                        margin: 10
                    }}
                    onChangeText={(newValue) => {
                        setAppId(newValue)
                    }}
                />

                <TextInput
                    placeholder="Enter Unit Id"
                    value={unitId}
                    style={{
                        borderBottomWidth: 1,
                        minWidth: 0,
                        margin: 10
                    }}
                    onChangeText={(newValue) => {
                        setUnitId(newValue)
                    }}
                />

                <View style={{
                    display: "flex",
                    flexDirection: "row"
                }}>
                    <GGButton title="init"
                        onPress={initializeSDK}
                    />
                    <GGButton title="destroy"
                        onPress={destroySDK}
                    />

                    <GGButton title="load and show Inter"
                        onPress={loadAdShowInter}
                    />

                </View>
                {isReady ? <GGAdviewWrapper
                    unitId={unitId}
                    style={{
                        height: undefined,
                        width: "100%",
                        aspectRatio: 16 / 9
                    }}
                /> : <ViewPlaceHolder />}

            </View>
        </>
    )
}