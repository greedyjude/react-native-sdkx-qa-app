import React from 'react';
import { GGAdView, GGAdviewWrapper } from 'react-native-sdkx';
import { Text, View, TextInput } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import { GGButton } from './Components/index.js';



export default function TemplateTest() {
    const [adHeight, setAdHeight] = React.useState(50)
    const [tempAdHeight, setTempAdHeight] = React.useState(50)

    const [adWidth, setAdWidth] = React.useState(100)
    const [tempAdWidth, setTempAdWidth] = React.useState(100)


    const [adMaxHeight, setAdMaxHeight] = React.useState(undefined)
    const [tempAdMaxHeight, setTempAdMaxHeight] = React.useState(undefined)

    const [adMaxWidth, setAdMaxWidth] = React.useState(undefined)
    const [tempAdMaxWidth, setTempAdMaxWidth] = React.useState(undefined)

    const adViewDimensionProps = [
        { label: "wrap", value: -2 },
        { label: "match", value: -1 }
    ]

    return (
        <>
            <View>
                <GGAdviewWrapper
                    key={Date.now()}
                    unitId="float-4732"
                    style={{
                        height: adHeight == -1 ? "100%" : adHeight,
                        width: adWidth == -1 ? "100%" : adWidth,
                        adsMaxHeight:adMaxHeight,
                        adsMaxWidth: adMaxWidth,
                    }}
                    onLayout={(event) => {
                        let { width, height } = event.nativeEvent.layout
                        console.log("onLayout", width, "X", height)
                    }}
                />
            </View>
            <View
                style={{
                    display: "flex",
                    flexDirection: "column",
                    position: "absolute",
                    bottom: 10
                }}
            >

                <View
                    style={{
                        display: "flex",
                        flexDirection: "row"
                    }}>
                    <TextInput
                        placeholder={"Width"}
                        style={{
                            borderBottomWidth: 1,
                            minWidth: 0,
                            margin: 10
                        }}
                        onChangeText={(newValue) => {
                            console.log("New Width", parseInt(newValue))
                            setTempAdWidth(parseInt(newValue))
                        }}
                    />

                    <TextInput
                        placeholder={"Max Width"}
                        style={{
                            borderBottomWidth: 1,
                            minWidth: 0,
                            margin: 10
                        }}

                        onChangeText={(newValue) => {
                            if((newValue.length) == 0){
                                setTempAdMaxWidth(undefined)
                                return
                            }
                            setTempAdMaxWidth(parseInt(newValue))
                        }}
                    />

                    <View
                        style={{
                            width: 40
                        }}
                    />

                    <TextInput
                        placeholder={"Height"}
                        style={{
                            borderBottomWidth: 1,
                            minWidth: 0,
                            margin: 10
                        }}
                        onChangeText={(newValue) => {
                            console.log("New Height", parseInt(newValue))
                            setTempAdHeight(parseInt(newValue))
                        }}
                    />

                    <TextInput
                        placeholder={"Max Height"}
                        style={{
                            borderBottomWidth: 1,
                            margin: 10
                        }}
                        onChangeText={(newValue) => {
                            if(newValue.length == 0){
                                setTempAdMaxHeight (undefined)
                                return
                            }
                            
                            setTempAdMaxHeight(parseInt(newValue))
                        }}
                    />


                </View>




                <View
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        textAlign: "center",
                        alignItems:"center"

                    }}
                >
                    <RadioForm
                        formHorizontalG={true}
                        buttonColor={"#2196f3"}
                        animation={true}
                        onPress={(value) => {
                            setTempAdWidth(parseInt(value))
                        }}

                        radio_props={adViewDimensionProps}



                    />
                    <View
                        style={{
                            width: 50
                        }}
                    />

                    <RadioForm
                        formHorizontalG={true}
                        buttonColor={"#2196f3"}
                        animation={true}
                        onPress={(value) => {
                            setTempAdHeight(parseInt(value))
                        }}
                        radio_props={adViewDimensionProps}

                    />


                </View>

                <View
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        flex: 6,
                        justifyContent: "center"

                    }}
                >

                    <Text
                        style={{
                            textAlign: "center",
                            flex: 2
                        }}>Width</Text>
                    <GGButton
                        style={{
                            flex: 2
                        }}
                        title="Load AD"
                        onPress={() => {
                            console.log("Setting values from temp values", tempAdWidth, "X", tempAdHeight)
                            console.log("Setting max values from temp values", tempAdMaxWidth, "X", tempAdMaxHeight)
                            setAdHeight(tempAdHeight)
                            setAdWidth(tempAdWidth)
                            setAdMaxHeight(tempAdMaxHeight)
                            setAdMaxWidth(tempAdMaxWidth)
                        }}
                    />
                    <Text
                        style={{
                            textAlign: "center",
                            flex: 2
                        }}
                    >Height</Text>

                </View>

                <View>

                </View>
            </View>

        </>
    )
}