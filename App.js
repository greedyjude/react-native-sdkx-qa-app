import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Router,Scene, Stack } from 'react-native-router-flux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import SDKX, { GGAdView } from 'react-native-sdkx';
import InterstitialMain from './Components/interstitial';
import First from './Components/interstitial/First';
import Second from './Components/interstitial/Second';
import Home from './Home';
import TemplateTest from './TemplateTest';
import Universal from './Universal';

export default function App() {
  return (
    <SafeAreaProvider>
      <Router>
        <Stack>
          <Scene key="home" component={Home} title="Home" initial />
          <Scene key="templateTest" component={TemplateTest} title="Template test" />
          <Scene key="interstitialMain" component={InterstitialMain} title="Interstitial Test" />
          <Scene key="universal" component={Universal} title="Universal" />
          <Scene key="interfirst" component={First} title="First Activity" />
          <Scene key="intersecond" component={Second} title="Second Activity" />
          </Stack>

      </Router>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
