import React from 'react';
import {Text,View,StatusBar,SafeAreaView,ScrollView,StyleSheet} from 'react-native';
import {GGButton, ViewPlaceHolder} from './Components/index.js'
import SDKX,{GGAdView} from 'react-native-sdkx'
import { Actions } from 'react-native-router-flux';

    function Home(){
        const [initState, setInitState] = React.useState("SDK not initialized")
        const [isInstallTrackingDisabled,setIsInstallTrackingDisabled] = React.useState(false)
        const [isSDKReady,setSDKReady] = React.useState(false)

        function initializeSDK() {
            SDKX.initWith({
                appId: "81939769",
                enableInstallTracking:!isInstallTrackingDisabled
                
            }).then(() => {
                setInitState("SDK Initialized")
                setSDKReady(true)
            }).catch((e) => {
                console.error(e)
                setInitState("SDK Failed to intialize")
                setSDKReady(false)
            })
        }
    
        function destroySDK() {
            SDKX.destroy()
            setInitState("SDK Destroyed")
            setSDKRead
        }
    
        function crashApp() {
            const a = {}
            console.log(a.should.crash)
        }
    
        function prefetchAds() {
            SDKX.prefetchAds({
                adUnits: ["float-5896", "float-5143", "float-5114", "float-5113"],
                onAdPrefetched: (unitID) => {
                    console.log("Ad Prefetched", unitID)
                },
                onAdPrefetchFailed: (unitID, e) => {
                    console.log("Ad Prefetch failed", unitID, e)
                },
                onPrefetchComplete: () => {
                    console.log("Ad prefetch complete")
                }
            })
        }
        return (
            <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
                <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}
                >

<Text>{initState}</Text>
                    <View style={styles.buttonContainer}>
                        <GGButton
                            title="Init"
                            onPress={initializeSDK}
                        />

                        <GGButton
                            title="RecyclerView"
                        />

                        <GGButton
                            title="Crash"
                            onPress={crashApp}
                        />

                        <GGButton
                            title="Destroy"
                            onPress={destroySDK}
                        />
                        
                        <GGButton
                            title="Interstitial"
                            onPress = {()=>{
                                Actions.interstitialMain()
                            }}
                        />
                        <GGButton
                            title="PrefetchAds"
                            onPress={prefetchAds}
                        />

                        <GGButton
                            title="Disable Install Tracking"
                            onPress = {() => {
                                setIsInstallTrackingDisabled(!isInstallTrackingDisabled)
                            }}
                        />

                        <GGButton title="Universal"
                        onPress={()=>{
                            Actions.universal()
                        }} />

                        <GGButton title="Template Test"
                        onPress={()=>{
                            Actions.templateTest()
                        }}/>
                    </View>
                    {isSDKReady?
                        <GGAdView
                        style={styles.adUnit}
                        unitId="float-4732"
                    />: <ViewPlaceHolder />
                    }
                    
                    {isSDKReady?
                        <GGAdView
                        style={styles.adUnit}
                        unitId="float-4731"
                    />: <ViewPlaceHolder />
                    }
                
                </ScrollView>
            </SafeAreaView>
            </>
        )
    }
// }

const styles = StyleSheet.create({
    scrollView: {
        height:"100%",
        width:"100%"
    },

    buttonContainer: {
        display: "flex",
        flexWrap: "wrap",
        flexDirection: "row",
        marginTop: 32,
        marginHorizontal: 16
    },

   

    adUnit: {
        marginTop: 20,
        width: "100%",
        aspectRatio: 16 / 9
    }
})
export default Home;